package sk.murin.library;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class Library {
    private final Font f = new Font("Corbel", Font.BOLD, 14);
    private final String[] Genres = {"-", "Drama", "Komedia", "Horror", "Rozpravka", "Dokument"};
    private final String[] Rating = {"-", "1/5 - zla", "2/5- cakal som viac", "3/5 - celkom dobra", "4/5 - velmi dobra", "5/5 - uplne super"};
    private final JTextField[] fields = new JTextField[3];
    private JComboBox comboBoxRating;
    private JComboBox comboBoxGenres;
    private DefaultTableModel modelTable;
    private String name, author, year, genre, rating;
    private JFrame frame;
    private JTable table;
    private final JPanel panelLeft = new JPanel();
    private  GridBagConstraints c;
    private  GridBagLayout gridbag;

    public Library() {
        setup();
    }

    private void setup() {
        frame = new JFrame("Kniznica");
        frame.setSize(900, 400);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        gridbag = new GridBagLayout();
        c = new GridBagConstraints();

        c.gridwidth = GridBagConstraints.BOTH;
        c.fill = GridBagConstraints.BOTH;
        c.weighty = 1.0;
        c.weightx = 1.0;
        gridbag.setConstraints(panelLeft, c);

        frame.setLayout(gridbag);
        frame.setLocationRelativeTo(null);
        frame.add(makePanelLeft());
        frame.add(makePanelRight());
        frame.setResizable(false);

        JMenu menu = new JMenu("Možnosti");
        menu.setFont(f);
        JMenuBar menuBar = new JMenuBar();
        menuBar.setFont(f);
        JMenuItem itemSave = new JMenuItem("Uložiť na plochu");
        itemSave.setFont(f);
        menuBar.add(menu);
        menuBar.setBackground(new Color(83, 203, 185));
        menu.add(itemSave);
        itemSave.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveToDesktop(e);
            }
        });
        frame.setJMenuBar(menuBar);
        frame.setVisible(true);
    }

    private JPanel makePanelLeft() {
        panelLeft.setLayout(null);
        panelLeft.setPreferredSize(new Dimension(300, 400));

        JLabel[] labels = new JLabel[6];
        for (int i = 0; i < labels.length; i++) {
            labels[i] = new JLabel();
            labels[i].setFont(f);
            labels[i].setForeground(Color.BLACK);
            panelLeft.add(labels[i]);
        }

        labels[0].setText("Pridaj Knihu");
        labels[0].setBounds(100, 6, 130, 35);
        labels[1].setText("Názov Knihy");
        labels[1].setBounds(15, 50, 100, 25);
        labels[2].setText("Autor Knihy");
        labels[2].setBounds(15, 100, 100, 25);
        labels[3].setText("Rok vydania");
        labels[3].setBounds(15, 150, 100, 25);
        labels[4].setText("Žáner Knihy");
        labels[4].setBounds(15, 200, 100, 25);
        labels[5].setText("Hodnotenie knihy");
        labels[5].setBounds(15, 250, 135, 25);


        DefaultListCellRenderer dlcr = new DefaultListCellRenderer();
        dlcr.setHorizontalAlignment(DefaultListCellRenderer.CENTER);


        for (int i = 0; i < fields.length; i++) {
            fields[i] = new JTextField();
            labels[i].setFont(f);
            labels[i].setForeground(Color.BLACK);
            panelLeft.add(fields[i]);

        }

        fields[0].setBounds(110, 50, 160, 28);
        fields[1].setBounds(110, 100, 160, 28);
        fields[2].setBounds(110, 150, 90, 28);


        comboBoxRating = new JComboBox(Rating);
        comboBoxRating.setBounds(130, 250, 150, 28);
        comboBoxRating.setFont(f);
        comboBoxRating.setRenderer(dlcr);
        panelLeft.add(comboBoxRating);

        comboBoxGenres = new JComboBox(Genres);
        comboBoxGenres.setBounds(145, 200, 110, 28);
        comboBoxGenres.setFont(f);
        comboBoxGenres.setSelectedIndex(0);

        comboBoxGenres.setRenderer(dlcr);
        panelLeft.add(comboBoxGenres);

        JButton btnSubmit = new JButton("Potvrd");
        btnSubmit.setBounds(85, 290, 140, 38);
        btnSubmit.setFont(new Font("Courier", Font.BOLD, 17));
        btnSubmit.addActionListener(e -> addBook());
        panelLeft.setBackground(new Color(83, 203, 185));
        panelLeft.add(btnSubmit);

        return panelLeft;
    }

    private JPanel makePanelRight() {
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(1, 1));
        panel.setBounds(panelLeft.getWidth(), 0, 600, 400);

        table = new JTable(0, 5);
        table.setFont(f);

        table.setDefaultEditor(Object.class, null);
        table.setRowHeight(22);

        table.setPreferredScrollableViewportSize(new Dimension
                (panelLeft.getWidth(), panel.getHeight()));

        table.setBackground(new Color(83, 203, 185));
        table.setForeground(Color.black);
        table.setFillsViewportHeight(true);

        Dimension tableSize = table.getPreferredSize();
        int x = tableSize.width;
        table.getColumnModel().getColumn(0).setPreferredWidth(Math.round(x * 0.15f));
        table.getColumnModel().getColumn(1).setPreferredWidth(Math.round(x * 0.15f));
        table.getColumnModel().getColumn(2).setPreferredWidth(Math.round(x * 0.15f));
        table.getColumnModel().getColumn(3).setPreferredWidth(Math.round(x * 0.25f));
        table.getColumnModel().getColumn(4).setPreferredWidth(Math.round(x * 0.30f));

        JTableHeader header = table.getTableHeader();
        TableColumnModel colMod = header.getColumnModel();
        TableColumn col0 = colMod.getColumn(0);
        col0.setResizable(false);
        col0.setHeaderValue("Názov");
        TableColumn col1 = colMod.getColumn(1);
        col1.setResizable(false);
        col1.setHeaderValue("Autor");
        TableColumn col2 = colMod.getColumn(2);
        col2.setResizable(false);
        col2.setHeaderValue("Rok vydania");
        TableColumn col3 = colMod.getColumn(3);
        col3.setResizable(false);
        col3.setHeaderValue("Žáner");
        TableColumn col4 = colMod.getColumn(4);
        col4.setResizable(false);
        col4.setHeaderValue("Hodnotenie");
        header.setFont(f);

        modelTable = (DefaultTableModel) table.getModel();

        ListSelectionModel model = table.getSelectionModel();
        model.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!model.isSelectionEmpty()) {
                    int selectedRow = model.getMinSelectionIndex();
                    int tableSelectedColumn = table.getSelectedColumn();
                    String valueFromInput = JOptionPane.showInputDialog(null, "Zadaj hodnotu","Zadaj",JOptionPane.INFORMATION_MESSAGE);
                    try {
                        if (!valueFromInput.isEmpty()) {
                            modelTable.setValueAt(valueFromInput, selectedRow, tableSelectedColumn);
                        }
                    } catch (NullPointerException ex) {
                    }
                }
            }
        });

        table.setGridColor(Color.black);
        table.grabFocus();
        table.getTableHeader().setReorderingAllowed(false);

        c.gridwidth = GridBagConstraints.REMAINDER;
        c.weightx = 2.0;
        gridbag.setConstraints(panel, c);

        panel.add(new JScrollPane(table));
        return panel;
    }

    private void reset() {
        fields[0].setText("");
        fields[1].setText("");
        fields[2].setText("");
        comboBoxRating.setSelectedIndex(0);
        comboBoxGenres.setSelectedIndex(0);
    }

    private void addBook() {
        boolean areDigits = false;
        if (fields[0].getText() != null && fields[1].getText() != null
                && fields[2].getText() != null && comboBoxRating.getSelectedItem() != "-" && comboBoxGenres.getSelectedItem() != "-") {
            year = String.valueOf(fields[2].getText());
            areDigits = false;
            for (int i = 0; i < year.length(); i++) {
                if (Character.isDigit(year.charAt(i))) {
                    areDigits = true;
                } else {
                    JOptionPane.showMessageDialog(null, "Zadaj správne hodnoty","Text do textových polí, číslo do roku",JOptionPane.INFORMATION_MESSAGE);
                    return;
                }
            }
        }
        if (areDigits) {
            modelTable.addRow(new Object[]{name, author, year, genre, rating});
            name = fields[0].getText();
            modelTable.setValueAt(name, 0, 0);

            author = fields[1].getText();
            modelTable.setValueAt(author, 0, 1);

            modelTable.setValueAt(year, 0, 2);
            genre = String.valueOf(comboBoxGenres.getSelectedItem());
            modelTable.setValueAt(genre, 0, 3);
            rating = String.valueOf(comboBoxRating.getSelectedItem());
            modelTable.setValueAt(rating, 0, 4);
            reset();
        }
    }

    public void saveToDesktop(ActionEvent evt) {
        final JFileChooser SaveAs = new JFileChooser();
        int returnVal = SaveAs.showSaveDialog(frame);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            try {
                File file = SaveAs.getSelectedFile();
                PrintWriter os = new PrintWriter(file + ".txt");
                for (int col = 0; col < table.getColumnCount(); col++) {
                    os.print(table.getColumnName(col) + "\t\t");
                }
                os.println();
                for (int row = 0; row < table.getRowCount(); row++) {
                    for (int col = 0; col < table.getColumnCount(); col++) {
                        os.print(table.getColumnName(col)+" ");
                        os.println(table.getValueAt(row, col));
                    }
                }
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
